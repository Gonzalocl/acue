#!/bin/bash

echo Total rows
wc -l movies.dat
echo Last ID
cut -d ':' -f 1 movies.dat | sort -n | tail -n 1
echo Unique IDs
cut -d ':' -f 1 movies.dat | sort -n | uniq | wc -l

echo Total rows
wc -l ra.train
echo Last ID
cut -d ':' -f 1 ra.train | sort -n | tail -n 1
echo Unique IDs
cut -d ':' -f 1 ra.train | sort -n | uniq | wc -l


